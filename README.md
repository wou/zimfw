# wou/zimfw

This is my personal collection of tweaks for the [zim][zim] configuration framework.
Well, at the minute, it's not really a collection just yet, because it only contains a single addition.

## Current features

- cross-platform "command not found" handler

## Installation

Add this to your `~/.zimrc`:

```
zmodule "https://codeberg.org/wou/zimfw"
```

Then run `zimfw install` and restart your shell to apply the changes.

[zim]: https://zimfw.sh
